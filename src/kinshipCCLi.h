#ifndef KINSHIPCCLI_H
#define KINSHIPCCLI_H

#include "empKin.h"

void CCLi(struct ALL_SNP_INFO all_snp_info, struct GENO_DATA geno_data, struct KINSHIP *kinship);
double CCLi_SimilarityIndex(int genotype1, int genotype2);
void chance_similarity(struct ALL_SNP_INFO *all_snp_info);

#endif
