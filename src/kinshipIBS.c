#include "kinshipIBS.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "nrutil.h"

/* calculate contribution of one SNP to kinship of all pairs of individuals group group1 and group2*/
void IBS_fraction(struct ALL_SNP_INFO all_snp_info, struct GENO_DATA geno_data, struct KINSHIP *kinship) {
  double f = 0;
  int *geno1 = geno_data.geno_g1;
  int *geno2 = geno_data.geno_g2;
  int group1 = geno_data.group1;
  int group2 = geno_data.group2;
  int sz_g1 = geno_data.sz_g1;
  int sz_g2 = geno_data.sz_g2;
  int ind1, ind2;
  if(group1 < group2) {
    for(ind1=1;ind1<=sz_g1;ind1++) {
      for(ind2=1;ind2<=sz_g2;ind2++){
        f = (2-abs(geno1[ind1]-geno2[ind2]))/2.0;
        kinship->r[ind1][ind2] = kinship->r[ind1][ind2] + f;
      }
    }
  }

  if(group1==group2) {
    for(ind1=1;ind1<=sz_g1;ind1++) {
      for(ind2=ind1;ind2<=sz_g1;ind2++){
        f = (2-abs(geno1[ind1]-geno2[ind2]))/2.0;
        kinship->r[ind1][ind2] = kinship->r[ind1][ind2] + f;
      }
    }
  }
}

void IBS_centered(struct ALL_SNP_INFO all_snp_info, struct GENO_DATA geno_data, struct KINSHIP *kinship) {
  double c_ibs = 0, p;
  int *geno1 = geno_data.geno_g1;
  int *geno2 = geno_data.geno_g2;
  int group1 = geno_data.group1;
  int group2 = geno_data.group2;
  int sz_g1 = geno_data.sz_g1;
  int sz_g2 = geno_data.sz_g2;
  int snp_i = geno_data.snp_i;
  int ind1, ind2;
  if(group1 < group2) {
    for(ind1=1;ind1<=sz_g1;ind1++) {
      for(ind2=1;ind2<=sz_g2;ind2++){
        p = all_snp_info.af[snp_i][2];
        c_ibs = (geno1[ind1]/2.0-p)*(geno2[ind2]/2.0-p);
        kinship->r[ind1][ind2] = kinship->r[ind1][ind2] + c_ibs;
      }
    }
  }
  if(group1==group2) {
    for(ind1=1;ind1<=sz_g1;ind1++) {
      for(ind2=ind1;ind2<=sz_g1;ind2++){
        p = all_snp_info.af[snp_i][2];
        c_ibs = (geno1[ind1]/2.0-p)*(geno2[ind2]/2.0-p);
        kinship->r[ind1][ind2] = kinship->r[ind1][ind2] + c_ibs;
      }
    }
  }
}

void IBS_standardized(struct ALL_SNP_INFO all_snp_info, struct GENO_DATA geno_data, struct KINSHIP *kinship) {
  double s_ibs = 0, p, var;
  int *geno1 = geno_data.geno_g1;
  int *geno2 = geno_data.geno_g2;
  int group1 = geno_data.group1;
  int group2 = geno_data.group2;
  int sz_g1 = geno_data.sz_g1;
  int sz_g2 = geno_data.sz_g2;
  int snp_i = geno_data.snp_i;
  int ind1, ind2;
  if(group1 < group2) {
    for(ind1=1;ind1<=sz_g1;ind1++) {
      for(ind2=1;ind2<=sz_g2;ind2++){
        p = all_snp_info.af[snp_i][2];
        var = all_snp_info.var[snp_i];
        s_ibs = (geno1[ind1]/2.0-p)*(geno2[ind2]/2.0-p)/var;
        kinship->r[ind1][ind2] = kinship->r[ind1][ind2] + s_ibs;
      }
    }
  }
  if(group1==group2) {
    for(ind1=1;ind1<=sz_g1;ind1++) {
      for(ind2=ind1;ind2<=sz_g1;ind2++){
        p = all_snp_info.af[snp_i][2];
        var = all_snp_info.var[snp_i];
        s_ibs = (geno1[ind1]/2.0-p)*(geno2[ind2]/2.0-p)/var;
        kinship->r[ind1][ind2] = kinship->r[ind1][ind2] + s_ibs;
      }
    }
  }
}
