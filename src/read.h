#ifndef READ_H
#define READ_H

#include <stdio.h>
#include "empKin.h"
#include <zlib.h>

void count_geno(char *filename, struct ALL_SNP_INFO *all_snp_info);
void read_tfam(char *filename, char **rankID2indID);
void count_snps(char *filename, struct ALL_SNP_INFO *all_snp_info);
void calculate_af(char *filename, struct ALL_SNP_INFO *all_snp_info, struct SNP_MEMORY_ALLO snp_memory_allo);
void addgeno(int *geno, int allele, int *y);
void allocate_memory(struct ALL_SNP_INFO *all_snp_info, struct SNP_MEMORY_ALLO *snp_memory_allo, struct GENO_DATA *geno_data, struct KINSHIP *kinship);
void free_memory(struct ALL_SNP_INFO *all_snp_info, struct SNP_MEMORY_ALLO *snp_memory_allo,struct GENO_DATA *geno_data, struct KINSHIP *kinship);
void read_genos(gzFile file, struct ALL_SNP_INFO *all_snp_info, struct GENO_DATA *geno_data, struct SNP_MEMORY_ALLO snp_memory_allo);
void calculate_var(char *filename, struct ALL_SNP_INFO *all_snp_info, int s_var, struct SNP_MEMORY_ALLO snp_memory_allo);
#endif
