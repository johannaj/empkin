#ifndef KINSHIPIBS_H
#define KINSHIPIBS_H

#include "empKin.h"

void IBS_fraction(struct ALL_SNP_INFO all_snp_info, struct GENO_DATA geno_data, struct KINSHIP *kinship);
void IBS_centered(struct ALL_SNP_INFO all_snp_info, struct GENO_DATA geno_data, struct KINSHIP *kinship);
void IBS_standardized(struct ALL_SNP_INFO all_snp_info, struct GENO_DATA geno_data, struct KINSHIP *kinship);

#endif
