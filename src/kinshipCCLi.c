#include "kinshipCCLi.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "nrutil.h"

/* calculate contribution of one SNP to CCLi kinship of all paris of individuals group group1 and group2*/
void CCLi(struct ALL_SNP_INFO all_snp_info, struct GENO_DATA geno_data, struct KINSHIP *kinship) {
  double u = all_snp_info.u_mean;
  double weights_sum = 0, r = 0, s=0;
  int *geno1 = geno_data.geno_g1;
  int *geno2 = geno_data.geno_g2;
  int group1 = geno_data.group1;
  int group2 = geno_data.group2;
  int sz_g1 = geno_data.sz_g1;
  int sz_g2 = geno_data.sz_g2;
  int ind1, ind2;
  if(group1 < group2) {
    for(ind1=1;ind1<=sz_g1;ind1++) {
      for(ind2=1;ind2<=sz_g2;ind2++){
        s = CCLi_SimilarityIndex(geno1[ind1],geno2[ind2]);
        kinship->r[ind1][ind2] = kinship->r[ind1][ind2] + (s-u)/(1.0-u);
      }
    }
  }
  if(group1==group2) {
    for(ind1=1;ind1<=sz_g1;ind1++) {
      for(ind2=ind1;ind2<=sz_g1;ind2++){
        s = CCLi_SimilarityIndex(geno1[ind1],geno2[ind2]);
        kinship->r[ind1][ind2] = kinship->r[ind1][ind2] + (s-u)/(1.0-u);
      }
    }
  }
}

/* similarity due to chance can be calculated from population allele frequencies
   for many loci the average is used
   u_locus = 2*(p_locus^2+q_locus^2)-(p_locus^3+q_locus^3) */
void chance_similarity(struct ALL_SNP_INFO *all_snp_info) {
  int i_snps, n_snps = all_snp_info->n_snps;
  double u = 0;
  double p, q;
  for(i_snps=1; i_snps<=n_snps; i_snps++){
    p = all_snp_info->af[i_snps][1];
    q = all_snp_info->af[i_snps][2];
    u = u + 2*(p*p+q*q) - (p*p*p+q*q*q);
  }
  u = u/n_snps;
  all_snp_info->u_mean = u;
}

double CCLi_SimilarityIndex(int genotype1, int genotype2) {
  int abs_difference = abs(genotype1 - genotype2);
  double s = -9.0;
  if(abs_difference == 0) {
    s = 1.0;
  }
  if(abs_difference == 1) {
    s = 0.75;
  }
  if(abs_difference == 2) {
    s = 0.0;
  }
  return s;
}
