#include "empKin.h"

#include "nrutil.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <getopt.h>
#include <zlib.h>

#include "kinshipCCLi.h"
#include "kinshipIBS.h"
#include "read.h"

struct OPT_STRUCT k_opt_struct;
int GROUPSZ = 100;

void init_opt_struct(struct IND_PAIR_DATA ind_pair_data, struct ALL_SNP_INFO all_snp_info, struct SNP_MEMORY_ALLO snp_memory_allo, struct GENO_DATA geno_data, struct KINSHIP kinship) {
	k_opt_struct.ind_pair_data = ind_pair_data;
	k_opt_struct.all_snp_info = all_snp_info;
	k_opt_struct.snp_memory_allo = snp_memory_allo;
	k_opt_struct.geno_data = geno_data;
	k_opt_struct.kinship= kinship;
}

int main (int argc, char **argv) {

		printf("\n"
		"  -------------------------------------------------------------\n"
		"  |                          empKin                           |\n"
		"  |                                                           |\n"
		"  |              Empirical estimation of kinship              |\n"
		"  |                                                           |\n"
		"  |                Version 0.2 - Jan  18, 2017                |\n"
		"  |                                                           |\n"
		"  |                  Copyright(C) 2015-2017                   |\n"
		"  |                   Johanna Jakobsdottir                    |\n"
		"  |                                                           |\n"
		"  |           http://bitbucket.org/johannaj/empKin            |\n"
		"  |                   License: GNU GPL v3                     |\n"
		"  -------------------------------------------------------------\n\n"
	);

	struct ALL_SNP_INFO all_snp_info;
	struct IND_PAIR_DATA ind_pair_data;
	struct SNP_MEMORY_ALLO snp_memory_allo;
	struct GENO_DATA geno_data;
	struct KINSHIP kinship;
	init_opt_struct(ind_pair_data,all_snp_info,snp_memory_allo,geno_data,kinship);

	char *pedfile, *genofile, *prefix;
	pedfile = (char*) malloc(MAXLEN);
	genofile = (char*) malloc(MAXLEN);
	prefix = (char*) malloc(MAXLEN);
	strcpy(prefix,"");
	strcpy(pedfile,"ped.txt");
	strcpy(genofile,"geno.txt");

	int pfile=0, gfile=0;

	const char* opt_string = "p:g:";
	static struct option long_options[] =
	{
		{"pedfile", required_argument,0,'p'},
		{"genofile", required_argument,0,'g'},
		{"prefix",required_argument,0,0},
		{"ccli",no_argument,0,0},
		{"ibs_f",no_argument,0,0},
		{"ibs_c",no_argument,0,0},
		{"ibs_s",no_argument,0,0},
		{"sample_var",no_argument,0,0},
		{"hwe_var",no_argument,0,0},
		{"groupsz",required_argument,0,0},
		{0,0,0,0}
	};

	int option_index = 0, c, estimator_code, variance_code;
	while (1) {
		c = getopt_long(argc,argv,opt_string,long_options, &option_index);
		if(c == -1) {
			break;
		}
		switch(c) {
			case 'p':
			strcpy(pedfile, optarg);
			printf("User specified pedigree and phenotype file: %s\n", pedfile);fflush(stdout);
			pfile = 1;
			break;
			case 'g':
			strcpy(genofile, optarg);
			printf("User specified genotype file: %s\n", genofile);fflush(stdout);
			gfile = 1;
			break;
			case 0:
			if (strcmp("prefix",long_options[option_index].name) == 0) {
				strcpy(prefix,optarg);
			}
			if (strcmp("ccli",long_options[option_index].name) == 0) {
				estimator_code = 1;
			}
			if (strcmp("ibs_f",long_options[option_index].name) == 0) {
				estimator_code = 2;
			}
			if (strcmp("ibs_c",long_options[option_index].name) == 0) {
				estimator_code = 3;
			}
			if (strcmp("ibs_s",long_options[option_index].name) == 0) {
				estimator_code = 4;
			}
			if (strcmp("sample_var",long_options[option_index].name) == 0) {
				variance_code = 1;
			}
			if (strcmp("hwe_var",long_options[option_index].name) == 0) {
				variance_code = 0;
			}
			if (strcmp("groupsz",long_options[option_index].name) == 0) {
				GROUPSZ = atoi(optarg);
			}
			break;
			default:
			exit(1);
		} // close switch
	} // close while

	char **rankID2indID;
	int n_typed = 0, n_snps, n_groups,i_snps;

	char *kinship_outfile;
	kinship_outfile = (char*) malloc(MAXLEN);
	strcpy(kinship_outfile, "");
	strcpy(kinship_outfile, prefix);
	if(strcmp("",prefix) == 0) {
		strcat(kinship_outfile,"kinship.txt");
	} else {
		char postfix[20];
		strcpy(postfix,"_kinship.txt");
		strcat(kinship_outfile,postfix);
	}

	count_geno(pedfile, &all_snp_info);
	n_typed = all_snp_info.n_typed;
	rankID2indID = (char**)malloc((n_typed+1) * sizeof(char*)); /* rankID2indID[unique rank ID] = unique ID */
	read_tfam(pedfile, rankID2indID);
	count_snps(genofile, &all_snp_info);
	n_snps = all_snp_info.n_snps;
	allocate_memory(&all_snp_info,&snp_memory_allo,&geno_data,&kinship);
	calculate_af(genofile, &all_snp_info,snp_memory_allo);
	if(variance_code == 1 && estimator_code == 4) { calculate_var(genofile, &all_snp_info,1,snp_memory_allo);}
	if(variance_code == 0 && estimator_code == 4) { calculate_var(genofile, &all_snp_info,0,snp_memory_allo);}
	double r;
	int ind_i, ind_j;
	n_groups = (int)ceil(n_typed/((double)GROUPSZ));
	int groupsz_last = n_typed % GROUPSZ;
	int group1, group2;
	FILE *outfile;

	int i,j;
	double *w = dvector(1,all_snp_info.n_snps);
	for(i=1;i<=all_snp_info.n_snps;i++) {
		w[i] = 1.0;
	}

	printf("n_groups = %d\n",n_groups);fflush(stdout);
	printf("GROUPSZ = %d\n",GROUPSZ);fflush(stdout);
	printf("GROUPSZ last = %d\n",groupsz_last);fflush(stdout);

	/* ====================================================================================== */
	/* CC Li estimator in Human Heredity 1993 43 */
	/* For diallelic variants, this is the same as the estimator of Wang in Genetics 2002 160 */
	/* estimator_code = 1 */
	/* ====================================================================================== */
	if(estimator_code == 1) {
		printf("\nUsing the CC Li estimator:\n");fflush(stdout);
		chance_similarity(&all_snp_info);
		outfile = fopen(kinship_outfile,"w");
		for(group1=1; group1 <= n_groups; group1++) {
			geno_data.group1 = group1;
			geno_data.ind1_g1 = 1+(group1-1)*GROUPSZ;
			if(group1 < n_groups || groupsz_last==0) { geno_data.sz_g1 = GROUPSZ;} else {geno_data.sz_g1=groupsz_last;}
			for(group2=group1; group2 <= n_groups; group2++) {
				geno_data.group2 = group2;
				geno_data.ind1_g2 = 1+(group2-1)*GROUPSZ;
				if(group2 < n_groups || groupsz_last==0) { geno_data.sz_g2 = GROUPSZ;} else {geno_data.sz_g2=groupsz_last;}
				/* intialize kinship estimates calculated iteratively */
				for(i=1;i<=GROUPSZ;i++){
					for(j=1;j<=GROUPSZ;j++) {
						kinship.r[i][j]=0.0;
					}
				}
				/* loop through all SNPs */
				/* open genotype file */
				gzFile file;
				file = gzopen(genofile,"r");
				for(i_snps=1;i_snps <= n_snps; i_snps++) {
					read_genos(file, &all_snp_info, &geno_data, snp_memory_allo);
					CCLi(all_snp_info,geno_data, &kinship);
				} // end for(i_snps=1;...)
				/* print kinship values to file */
				if(group1==group2) {
					for(ind_i=1;ind_i<=geno_data.sz_g1;ind_i++) {
						for(ind_j=ind_i;ind_j<=geno_data.sz_g2;ind_j++) {
							fprintf(outfile,"%s %s %lf\n",rankID2indID[ind_i+(group1-1)*GROUPSZ],rankID2indID[ind_j+(group1-1)*GROUPSZ],kinship.r[ind_i][ind_j]/n_snps);
						}
					}
				}
				if(group1<group2) {
					for(ind_i=1;ind_i<=geno_data.sz_g1;ind_i++) {
						for(ind_j=1;ind_j<=geno_data.sz_g2;ind_j++) {
							fprintf(outfile,"%s %s %lf\n",rankID2indID[ind_i+(group1-1)*GROUPSZ],rankID2indID[ind_j+(group2-1)*GROUPSZ],kinship.r[ind_i][ind_j]/n_snps);
						}
					}
				}
				gzclose(file);
			} // end for(group2=group1;...)
		} // end for(group1=1;...)
		/* done printing kinship values to file */
		fclose(outfile);
	}

	/* ====================================================================================== */
	/* IBS_f is an estimator based on an average fraction of alleles IBS */
	/* It is the same as the EMMA/EMMAX estimator w/ equal weights */
	/* estimator_code = 2 */
	/* ====================================================================================== */
	if(estimator_code == 2) {
		printf("\nUsing fraction IBS estimator:\n");fflush(stdout);
		outfile = fopen(kinship_outfile,"w");
		for(group1=1; group1 <= n_groups; group1++) {
			geno_data.group1 = group1;
			geno_data.ind1_g1 = 1+(group1-1)*GROUPSZ;
			if(group1 < n_groups || groupsz_last==0) { geno_data.sz_g1 = GROUPSZ;} else {geno_data.sz_g1=groupsz_last;}
			for(group2=group1; group2 <= n_groups; group2++) {
				geno_data.group2 = group2;
				geno_data.ind1_g2 = 1+(group2-1)*GROUPSZ;
				if(group2 < n_groups || groupsz_last==0) { geno_data.sz_g2 = GROUPSZ;} else {geno_data.sz_g2=groupsz_last;}
				/* intialize kinship estimates calculated iteratively */
				for(i=1;i<=GROUPSZ;i++){
					for(j=1;j<=GROUPSZ;j++) {
						kinship.r[i][j]=0.0;
					}
				}
				/* loop through all SNPs */
				/* open genotype file */
				gzFile file;
				file = gzopen(genofile,"r");
				for(i_snps=1;i_snps <= n_snps; i_snps++) {
					read_genos(file, &all_snp_info, &geno_data, snp_memory_allo);
					IBS_fraction(all_snp_info, geno_data, &kinship);
				} // end for(i_snps=1;...)
				/* print kinship values to file */
				if(group1==group2) {
					for(ind_i=1;ind_i<=geno_data.sz_g1;ind_i++) {
						for(ind_j=ind_i;ind_j<=geno_data.sz_g2;ind_j++) {
							fprintf(outfile,"%s %s %lf\n",rankID2indID[ind_i+(group1-1)*GROUPSZ],rankID2indID[ind_j+(group1-1)*GROUPSZ],kinship.r[ind_i][ind_j]/n_snps);
						}
					}
				}
				if(group1<group2) {
					for(ind_i=1;ind_i<=geno_data.sz_g1;ind_i++) {
						for(ind_j=1;ind_j<=geno_data.sz_g2;ind_j++) {
							fprintf(outfile,"%s %s %lf\n",rankID2indID[ind_i+(group1-1)*GROUPSZ],rankID2indID[ind_j+(group2-1)*GROUPSZ],kinship.r[ind_i][ind_j]/n_snps);
						}
					}
				}
				gzclose(file);
			} // end for(group2=group1;...)
		} // end for(group1=1;...)
		/* done printing kinship values to file */
		fclose(outfile);
	}

	/* ====================================================================================== */
	/* IBS_c is an estimator of based on product of "centered" genotypes */
	/* same as the "centered" matrix used by GEMMA */
	/* estimator_code = 3 */
	/* ====================================================================================== */
	if(estimator_code == 3) {
		printf("\nUsing centered IBS estimator:\n");fflush(stdout);
		outfile = fopen(kinship_outfile,"w");
		for(group1=1; group1 <= n_groups; group1++) {
			geno_data.group1 = group1;
			geno_data.ind1_g1 = 1+(group1-1)*GROUPSZ;
			if(group1 < n_groups || groupsz_last==0) { geno_data.sz_g1 = GROUPSZ;} else {geno_data.sz_g1=groupsz_last;}
			for(group2=group1; group2 <= n_groups; group2++) {
				geno_data.group2 = group2;
				geno_data.ind1_g2 = 1+(group2-1)*GROUPSZ;
				if(group2 < n_groups || groupsz_last==0) { geno_data.sz_g2 = GROUPSZ;} else {geno_data.sz_g2=groupsz_last;}
				/* intialize kinship estimates calculated iteratively */
				for(i=1;i<=GROUPSZ;i++){
					for(j=1;j<=GROUPSZ;j++) {
						kinship.r[i][j]=0.0;
					}
				}
				/* loop through all SNPs */
				/* open genotype file */
				gzFile file;
				file = gzopen(genofile,"r");
				for(i_snps=1;i_snps <= n_snps; i_snps++) {
					read_genos(file, &all_snp_info, &geno_data, snp_memory_allo);
					geno_data.snp_i = i_snps;
					IBS_centered(all_snp_info, geno_data, &kinship);
				} // end for(i_snps=1;...)
				/* print kinship values to file */
				if(group1==group2) {
					for(ind_i=1;ind_i<=geno_data.sz_g1;ind_i++) {
						for(ind_j=ind_i;ind_j<=geno_data.sz_g2;ind_j++) {
							fprintf(outfile,"%s %s %lf\n",rankID2indID[ind_i+(group1-1)*GROUPSZ],rankID2indID[ind_j+(group1-1)*GROUPSZ],kinship.r[ind_i][ind_j]/n_snps);
						}
					}
				}
				if(group1<group2) {
					for(ind_i=1;ind_i<=geno_data.sz_g1;ind_i++) {
						for(ind_j=1;ind_j<=geno_data.sz_g2;ind_j++) {
							fprintf(outfile,"%s %s %lf\n",rankID2indID[ind_i+(group1-1)*GROUPSZ],rankID2indID[ind_j+(group2-1)*GROUPSZ],kinship.r[ind_i][ind_j]/n_snps);
						}
					}
				}
				gzclose(file);
			} // end for(group2=group1;...)
		} // end for(group1=1;...)
		/* done printing kinship values to file */
		fclose(outfile);
	}

	/* ====================================================================================== */
	/* IBS_s is an estimator of based on product of "standardized" genotypes product */
	/* same as the "standardized" matrix used by GEMMA and ROADTRIPS */
	/* the "centered" genotype product is standardized by some estimator of the variance of  */
	/* the genotypes: e.g. sample variance or variance calculated from alllele freq. based on HWE*/
	/* estimator_code = 4 */
	/* ====================================================================================== */
	if(estimator_code == 4) {
		printf("\nUsing standardized IBS estimator:\n");fflush(stdout);
		outfile = fopen(kinship_outfile,"w");
		for(group1=1; group1 <= n_groups; group1++) {
			geno_data.group1 = group1;
			geno_data.ind1_g1 = 1+(group1-1)*GROUPSZ;
			if(group1 < n_groups || groupsz_last==0) { geno_data.sz_g1 = GROUPSZ;} else {geno_data.sz_g1=groupsz_last;}
			for(group2=group1; group2 <= n_groups; group2++) {
				geno_data.group2 = group2;
				geno_data.ind1_g2 = 1+(group2-1)*GROUPSZ;
				if(group2 < n_groups || groupsz_last==0) { geno_data.sz_g2 = GROUPSZ;} else {geno_data.sz_g2=groupsz_last;}
				/* intialize kinship estimates calculated iteratively */
				for(i=1;i<=GROUPSZ;i++){
					for(j=1;j<=GROUPSZ;j++) {
						kinship.r[i][j]=0.0;
					}
				}
				/* loop through all SNPs */
				/* open genotype file */
				gzFile file;
				file = gzopen(genofile,"r");
				for(i_snps=1;i_snps <= n_snps; i_snps++) {
					read_genos(file, &all_snp_info, &geno_data, snp_memory_allo);
					geno_data.snp_i = i_snps;
					IBS_standardized(all_snp_info, geno_data, &kinship);
				} // end for(i_snps=1;...)
				/* print kinship values to file */
				if(group1==group2) {
					for(ind_i=1;ind_i<=geno_data.sz_g1;ind_i++) {
						for(ind_j=ind_i;ind_j<=geno_data.sz_g2;ind_j++) {
							fprintf(outfile,"%s %s %lf\n",rankID2indID[ind_i+(group1-1)*GROUPSZ],rankID2indID[ind_j+(group1-1)*GROUPSZ],kinship.r[ind_i][ind_j]/n_snps);
						}
					}
				}
				if(group1<group2) {
					for(ind_i=1;ind_i<=geno_data.sz_g1;ind_i++) {
						for(ind_j=1;ind_j<=geno_data.sz_g2;ind_j++) {
							fprintf(outfile,"%s %s %lf\n",rankID2indID[ind_i+(group1-1)*GROUPSZ],rankID2indID[ind_j+(group2-1)*GROUPSZ],kinship.r[ind_i][ind_j]/n_snps);
						}
					}
				}
				gzclose(file);
			} // end for(group2=group1;...)
		} // end for(group1=1;...)
		/* done printing kinship values to file */
		fclose(outfile);
	}
	/* ====================================================================================== */

	free(pedfile);
	free(genofile);
	free_dvector(w,1,all_snp_info.n_snps);
	free_memory(&all_snp_info,&snp_memory_allo,&geno_data,&kinship);
	return 0;

} // end main
