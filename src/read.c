#include "read.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "nrutil.h"

/* function to count number of genotyped individuals
assume file is in the *.tfam format of plink:
no header, columns have famID, indID, moID, faID, sex, trait */
void count_geno(char *filename, struct ALL_SNP_INFO *all_snp_info) {
	int ind, fam, sex, n_typed=0;
	double trait = 0;
	char *fam_id, *ind_id, *fa, *mo;
	fam_id = (char*) malloc(MAXLEN);
	ind_id = (char*) malloc(MAXLEN);
	fa = (char*) malloc(MAXLEN);
	mo = (char*) malloc(MAXLEN);

	/* open file */
	FILE *pedfile;
	pedfile = fopen(filename, "r");
	if(pedfile == NULL) {
		printf("Can't open file with list of genotyped individuals\n");
		exit(1);
	}

	/* read through the file once to count n_typed */
	while(!feof(pedfile)) {
		fscanf(pedfile,"%s %s %s %s %d %lf \n", fam_id, ind_id, fa, mo, &sex, &trait);
		n_typed++;
	}

	all_snp_info->n_typed = n_typed;

	fclose(pedfile);
	free(fam_id);
	free(ind_id);
	free(fa);
	free(mo);

} /* end function count_geno */

/* function to read in the ordered IDs of individuals in genotype file
assume file is in the *.tfam format of plink:
no header, columns have famID, indID, moID, faID, sex, trait */
void read_tfam(char *filename, char **rankID2indID) {
	int ind, fam, sex, i_typed=0;
	double trait = 0;
	char *fam_id, *ind_id, *fa, *mo;
	fam_id = (char*) malloc(MAXLEN);
	ind_id = (char*) malloc(MAXLEN);
	fa = (char*) malloc(MAXLEN);
	mo = (char*) malloc(MAXLEN);

	/* open file */
	FILE *pedfile;
	pedfile = fopen(filename, "r");
	if(pedfile == NULL) {
		printf("Can't open pedigree and trait file\n");
		exit(1);
	}

	/* read through the file and save ind IDs in the order they appear in */
	while(!(feof(pedfile))) {
		fscanf(pedfile,"%s %s %s %s %d %lf ", fam_id, ind_id, fa, mo, &sex, &trait);
		i_typed++; /* add current individual to the count */
		rankID2indID[i_typed] = strdup(ind_id);
	}

	fclose(pedfile);
	free(fam_id);
	free(ind_id);
	free(fa);
	free(mo);
} /* end function read_tfam */

/* function to count number of snps
assume file is in the *.tped format of plink:
no header, columns have chr, markerid, cm, bp, snp1_a1, snp1_a2, ..., snpi_a1 snpi_a2 */
void count_snps(char *filename, struct ALL_SNP_INFO *all_snp_info) {

	/* open file */
	gzFile file;
	file = gzopen(filename, "r");
	if(file == NULL) {
		printf("ERROR: Can't open genotype data file\n");
		exit(1);
	}

	int car=0, n_snps=0;
	/* read through the file once to count n_snps */
	while (car != EOF) {
		car = gzgetc(file);
		if(car == '\n') {
			n_snps++;
		}
	}

	gzclose(file);

	all_snp_info->n_snps = n_snps;

} /* end function count_snps */

/* calculate_af of each SNP*/
void calculate_af(char *filename, struct ALL_SNP_INFO *all_snp_info, struct SNP_MEMORY_ALLO snp_memory_allo) {

	/* open file */
	gzFile file;
	file = gzopen(filename,"r");

	int n_typed = all_snp_info->n_typed;
	int n_snps = all_snp_info->n_snps;
	int max_char = 4*n_typed+4*MAXLEN;
	int n_read=0; /* to keep track of how much of the line has been read with sscanf */
	int n_read_tmp=0; /* to store how much was read in the last call to sscanf */
	int i_snps, i_typed, n_geno = 0;
	int cm, bp;
	double af;
	int *geno; geno = snp_memory_allo.geno;
	char *chr; chr = snp_memory_allo.chr;
	char *markerid; markerid = snp_memory_allo.markerid;
	char *line; line = snp_memory_allo.line;

	for(i_snps=1;i_snps<=n_snps;i_snps++) {
		af=0.0;
		gzgets(file,line,max_char);
		n_geno = 0;
		n_read=0;
		n_read_tmp=0;
		sscanf(line, "%s %s %d %d %n", chr, markerid, &cm, &bp, &n_read_tmp);
		n_read += n_read_tmp; /* update how much of the string has been read by sscanf */
		for(i_typed=1;i_typed<=n_typed;i_typed++) {
			sscanf(line+n_read, "%1d %1d %n", &geno[1], &geno[2], &n_read_tmp);
			n_read += n_read_tmp;
			if(geno[1]!=0 && geno[2]!=0) {
				n_geno++;
				af = af + (geno[1]-1) + (geno[2]-1); //count no. of "2" alleles
			}
		}
		af = af/(2*n_geno);
		all_snp_info->af[i_snps][1] = 1.0-af;
		all_snp_info->af[i_snps][2] = af;
	}

	gzclose(file);
}

/* function to recode the genotype into an addtive model, allele = non-risk allele
assumes markers have only 2 alleles (e.g. SNPs) */
void addgeno(int *geno, int allele, int *y) {
	if(geno[1] == allele && geno[2] == allele) {
		*y = 0;
	}
	if(geno[1] == allele && geno[2] != allele) {
		*y = 1;
	}
	if(geno[2] == allele && geno[1] != allele) {
		*y = 1;
	}
	if(geno[1] != allele && geno[2] != allele) {
		*y = 2;
	}
	if(geno[1] == 0 && geno[2] == 0) {
		*y = -9;
	}
}

/* allocate memory of all_snp_info allele freq and weight vectors */
void allocate_memory(struct ALL_SNP_INFO *all_snp_info, struct SNP_MEMORY_ALLO *snp_memory_allo, struct GENO_DATA *geno_data, struct KINSHIP *kinship){
	int n_snps = all_snp_info->n_snps;
	all_snp_info->af = dmatrix(1,n_snps,1,2);
	all_snp_info->w = dvector(1,n_snps);
	all_snp_info->var = dvector(1,n_snps);
	int n_typed = all_snp_info->n_typed;
	int max_char = 4*n_typed+4*MAXLEN;
	snp_memory_allo->geno = ivector(1,2);
	snp_memory_allo->chr = (char*) malloc(4);
	snp_memory_allo->markerid = (char*) malloc(MAXLEN);
	snp_memory_allo->line = (char*) malloc(max_char);
	geno_data->geno_g1 = ivector(1,GROUPSZ);
	geno_data->geno_g2 = ivector(1,GROUPSZ);
	kinship->r = dmatrix(1,GROUPSZ,1,GROUPSZ);
}

/* free memory of all_snp_info allele freq and weight vectors */
void free_memory(struct ALL_SNP_INFO *all_snp_info, struct SNP_MEMORY_ALLO *snp_memory_allo,struct GENO_DATA *geno_data, struct KINSHIP *kinship){
	int n_snps = all_snp_info->n_snps;
	free_dmatrix(all_snp_info->af,1,n_snps,1,2);
	free_dvector(all_snp_info->w,1,n_snps);
	free_dvector(all_snp_info->var,1,n_snps);
	free_ivector(snp_memory_allo->geno,1,2);
	free(snp_memory_allo->chr);
	free(snp_memory_allo->markerid);
	free(snp_memory_allo->line);
	free_ivector(geno_data->geno_g1,1,GROUPSZ);
	free_ivector(geno_data->geno_g2,1,GROUPSZ);
	free_dmatrix(kinship->r,1,GROUPSZ,1,GROUPSZ);
}

/* read genotypes of many individuals for one SNP in two groups */
void read_genos(gzFile file, struct ALL_SNP_INFO *all_snp_info, struct GENO_DATA *geno_data, struct SNP_MEMORY_ALLO snp_memory_allo){

	int n_typed = all_snp_info->n_typed;
	int n_snps = all_snp_info->n_snps;
	int max_char = 4*n_typed+4*MAXLEN;
	int n_read; /* to keep track of how much of the line has been read with sscanf */
	int n_read_tmp; /* to store how much was read in the last call to sscanf */
	int i_snps, i_typed, n_geno = 0, i_ind;
	int cm, bp;
	double af;
	int *geno; geno = snp_memory_allo.geno;
	char *chr; chr = snp_memory_allo.chr;
	char *markerid; markerid = snp_memory_allo.markerid;
	char *line; line = snp_memory_allo.line;
	int ind1 = geno_data->ind1_g1;
	int ind2 = geno_data->ind1_g2;
	int sz_g1 = geno_data->sz_g1;
	int sz_g2 = geno_data->sz_g2;

	// read individuals in first group
	int allele = 1;
	gzgets(file,line,max_char);
	n_geno = 0;
	n_read=0;
	n_read_tmp=0;
	sscanf(line, "%s %s %d %d %n", chr, markerid, &cm, &bp, &n_read);
	n_read = n_read+(ind1-1)*4;
	sscanf(line+n_read, "%1d %1d %n",&geno[1], &geno[2], &n_read_tmp);
	n_read += n_read_tmp;
	int i=1;
	addgeno(geno,allele,&geno_data->geno_g1[i]);
	for(i_ind=1+ind1;i_ind<=sz_g1+ind1;i_ind++) {
		i++;
		sscanf(line+n_read,"%1d %1d %n",&geno[1],&geno[2],&n_read_tmp);
		n_read += n_read_tmp;
		addgeno(geno,allele,&geno_data->geno_g1[i]);
	}

	// read individuals in second group, if same group use pointer
	if(ind1==ind2) {
		//geno_data->geno_g2 = geno_data->geno_g1;
		memcpy(geno_data->geno_g2, geno_data->geno_g1, (GROUPSZ+1)* sizeof(int));
	} else {
		n_geno = 0;
		n_read=0;
		n_read_tmp=0;
		sscanf(line, "%s %s %d %d %n", chr, markerid, &cm, &bp, &n_read);
		n_read = n_read+(ind2-1)*4; // find where to start reading
		sscanf(line+n_read, "%1d %1d %n",&geno[1], &geno[2], &n_read_tmp);
		n_read += n_read_tmp;
		i=1;
		//addgeno(geno,allele,&geno_data->geno_g2[ind2]);
		addgeno(geno,allele,&geno_data->geno_g2[i]);
		for(i_ind=1+ind2;i_ind<=sz_g2+ind2;i_ind++) {
			i++;
			sscanf(line+n_read,"%1d %1d %n",&geno[1],&geno[2],&n_read_tmp);
			n_read += n_read_tmp;
			addgeno(geno,allele,&geno_data->geno_g2[i]);
		}
	}
}

/* calculate_var of each SNP*/
void calculate_var(char *filename, struct ALL_SNP_INFO *all_snp_info, int s_var, struct SNP_MEMORY_ALLO snp_memory_allo) {

	/* open file */
	gzFile file;
	file = gzopen(filename,"r");

	int n_typed = all_snp_info->n_typed;
	int n_snps = all_snp_info->n_snps;
	int i_snps, i_typed, n_geno = 0;
	int *geno; geno = snp_memory_allo.geno;
	double var = 0.0, af, add;

	if(s_var == 1) {

		int n_typed = all_snp_info->n_typed;
		int n_snps = all_snp_info->n_snps;
		int max_char = 4*n_typed+4*MAXLEN;
		int n_read=0; /* to keep track of how much of the line has been read with sscanf */
		int n_read_tmp=0; /* to store how much was read in the last call to sscanf */
		int i_snps, i_typed, n_geno = 0;
		int cm, bp;
		double af;
		int *geno; geno = snp_memory_allo.geno;
		char *chr; chr = snp_memory_allo.chr;
		char *markerid; markerid = snp_memory_allo.markerid;
		char *line; line = snp_memory_allo.line;

		for(i_snps=1;i_snps<=n_snps;i_snps++) {
			af=all_snp_info->af[i_snps][2];
			gzgets(file,line,max_char);
			n_geno = 0;
			n_read=0;
			n_read_tmp=0;
			sscanf(line, "%s %s %d %d %n", chr, markerid, &cm, &bp, &n_read_tmp);
			n_read += n_read_tmp; /* update how much of the string has been read by sscanf */
			var = 0.0;
			for(i_typed=1;i_typed<=n_typed;i_typed++) {
				sscanf(line+n_read, "%1d %1d %n", &geno[1], &geno[2], &n_read_tmp);
				n_read += n_read_tmp;
				if(geno[1]!=0 && geno[2]!=0) {
					n_geno++;
					add = ((geno[1]-1)+(geno[2]-1))/2.0; //prop of "2" alleles (i.e. additive coding 0,0.5,1)
					var = var + add*add;
				}
			}
			var = var/n_geno-af*af;
			all_snp_info->var[i_snps] = var;
		}
	}

	if(s_var==0) {
		for(i_snps=1;i_snps<=n_snps;i_snps++) {
			af=all_snp_info->af[i_snps][2];
			all_snp_info->var[i_snps] = 0.5*af*(1-af);
		}
	}
	gzclose(file);
}
