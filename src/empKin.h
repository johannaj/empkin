#ifndef EMPKIN_H
#define EMPKIN_H

#define MAXLEN 1024
#define MISSVAL -9.0
#define NUMTOL 1e-6

extern int GROUPSZ;

/* IND_PAIR_DATA stores genotype data for one pair of individuals at a time */
struct IND_PAIR_DATA {
  int *y1, *y2;   /* Genotypes of two individuals including missing = -9.0, y1[i] = genotype at SNP i */
  double *s_12;    /* similarity index, missing = -9.0, s_12[i] similarity index of two individuals at SNP i */
};

/* ALL_SNP_INFO has information that do not change across individual pairs */
struct ALL_SNP_INFO {
  double **af;     /* frequency of allele "1" and "2"; alleles: 1,2; (additive) genotypes: 0,1,2,NA
                    af[i][1] = freq of allele "1" of SNP i, af[i][2] = freq of allele "2" of SNP i */
  double *w;      /* w[i] = weight of SNP in overall estimator */
  double *var;    /* variance estimator of each SNP, maybe used as weights */
  double w_sum;   /* w_sum = SUM[ w[i] ], sum of the weights w[i] over all SNPs */
  int n_typed;   /* n_typed = number of individuals in genotype file (.tfam plink style)*/
  int n_snps;   /* number of SNPs */
  double u_mean;  /* mean over all SNPs of u = 1/N_SNPs x SUM[ 2(p_1^2+p_0^2) - (p_1^3+p_0^3)] */
};

struct SNP_MEMORY_ALLO {
  int *geno;
  char *chr, *markerid, *line;
};

struct GENO_DATA {
    int *geno_g1;  /* genotypes for all individuals in group1 for one snp */
    int *geno_g2; /* genotypes for all individuals in group2 for one snp */
    int sz_g1;    /* sz of group1, usually GROUPSZ except for last group */
    int sz_g2;    /* sz of group2, usually GROUPSZ except for last group */
    int group1;   /* no of group1 */
    int group2;   /* no of group2 */
    int ind1_g1;   /* no of first ind in group 1 as indexed over all inds */
    int ind1_g2;   /* no of first ind in group 2 as indexed over all inds */
    int snp_i;    /* no of the snp currently stored in geno_g1 and geno_g2 */
};

struct KINSHIP {
    double **r;    /* estimated kinship coefficients of all indviduals in group1 and group2 of sz GROUPSZ each */
};

struct OPT_STRUCT {
    struct ALL_SNP_INFO all_snp_info;
    struct IND_PAIR_DATA ind_pair_data;
    struct SNP_MEMORY_ALLO snp_memory_allo;
    struct GENO_DATA geno_data;
    struct KINSHIP kinship;
};

#endif
