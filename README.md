empKin implements empirical estimators of relatedness between two individuals based on SNP genotype data

### What is this repository for? ###

* This is a developmental repository, which always has the most up-to-date version of empKin.

### References ###

* Formulas and details on implemented estimators is in the documentation.

### Funding ###
Funding for the development of empKin was provided by

* The Icelandic Heart Association.
* The Icelandic Research Fund (130726-051, 130726-052, 130726-053 to Johanna Jakobsdottir).

### License ###
The empKin program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.